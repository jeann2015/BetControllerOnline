<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MUsusariosSending extends Model
{
    protected $table = 'm_ususarios_sendings';

    protected $fillable = [
        'user_id'
    ];
}
