<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MUsuarios extends Model
{
    protected $connection = 'mysqle';
    protected $table = 'm_usuarios';

    protected $fillable = [
        'nombre','apellido','email','id_usuario','login'
    ];

//    protected $hidden =['login','inactivo','clave','fecha_nacimiento','fecha_registro','telefono','id_estado',
//    'ciudad','direccion','celular','codigo_postal','total_cuenta', 'total_juego','total_disponible','maximo_jugadas',
//    'proporcion','limite_parleys','limite_directa','limite_tickets','limite_diario','limite_jugada_gratis',
//    'permite_jugar_gratis','boletin','cedula','id_plan','desactivar_promociones','ip_registro','limite_cobro',
//    'proporcion_limite_cobro','parley_unico','mensaje_entrada','jugada_gratis_comienzo_diario',
//    'maximo_deposito','minimo_deposito','maximo_retiro','minimo_retiro','minimo_jugadas_ticket'];

}
