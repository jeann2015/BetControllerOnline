<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
       protected $table="clients";
       protected $fillable = [
             'name','id_document','id_user','address','phone'
         ];
}
