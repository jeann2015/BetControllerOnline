<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
  protected $table="payments";
  protected $fillable = [
        'id_user','id_client','amount_payment','amount_current_loan'
    ];
}
