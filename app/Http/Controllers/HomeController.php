<?php

namespace App\Http\Controllers;


use App\Modules;

use App\Audits;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $iduser = \Auth::id();
        $audits = new Audits;
        $m = new Modules();
        $audits->save_audits('Login Aplication');
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        return view('home',compact('module_principals','module_menus'));
    }
}
