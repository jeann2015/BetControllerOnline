<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Clients;
use App\Modules;
use App\Audits;

class PaymentsController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
      $module = new Modules;
      $iduser = \Auth::id();
      $url = $request->path();
      $user_access = $module->accesos($iduser,$url);
      // $clients = Clients::all();
      return view('payments.index',compact('user_access'));
    }
}
