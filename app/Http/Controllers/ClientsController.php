<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Clients;
use App\Modules;
use App\Audits;

class ClientsController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
      $module = new Modules;
      $iduser = \Auth::id();
      $url = $request->path();
      $user_access = $module->accesos($iduser,$url);
      $clients = Clients::all();
      return view('clients.index',compact('clients','user_access'));
    }

    public function add(Request $request)
    {
      return view('clients.add');
    }

    public function news(Request $request)
		{
      
		  $audits = new Audits;
      $iduser = \Auth::id();
    	$client = Clients::create([
        'id_user'=>$iduser,
    		'name'=>$request->name,
    		'phone'=>$request->phone,
    		'address'=>$request->address,
        'id_document'=>$request->id_document]);

			$audits->save_audits('New Clients: '.$request->name);

    	return redirect('clients');

    }

    public function edit(Request $request)
		{
    	$clients = Clients::find($request->id);
    	return view('clients.mod',compact('clients'));
    }

    public function update(Request $request)
		{
      $iduser = \Auth::id();
    	$audits = new Audits;
    	$clients = Clients::find($request->id);
    	$clients->name = $request->name;
    	$clients->id_document = $request->id_document;
      $clients->address = $request->address;
    	$clients->phone = $request->phone;
    	$clients->id_user = $iduser;
    	$clients->save();
			$audits->save_audits('Update Clients: '.$request->id."-".$request->name);
    	return redirect('clients');

    }

    public function delete(Request $request)
		{
			$clients = Clients::find($request->id);
			return view('clients.del',compact('clients'));
    }

    public function destroy(Request $request)
		{
			$audits = new Audits;
			$client = Clients::find($request->id);
			$audits->save_audits('Delete Client: '.$request->id."-".$client->name);
			$client->delete();
			return redirect('clients');

    }

}
