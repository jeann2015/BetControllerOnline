<?php

namespace App\Http\Controllers;

use App\Modules;
use App\MUsuarios;
use App\MUsusariosSending;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Carbon;


class SendingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $MUsuarios = MUsuarios::all()->limit(3);

        $MUsusariosSending = MUsusariosSending::all();
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $modules = Modules::all();
        return view('sending.index', compact('MUsusariosSending', 'MUsuarios', 'modules', 'user_access'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function sending(Request $request)
    {

        $MUsuarios = MUsuarios::all();
        foreach ($MUsuarios as $MUsuario) {

            $when = Carbon\Carbon::now()->addMinutes(5);
            $id_usu='enviar'.$MUsuario->id_usuario;
            $id_usu_value = $request->$id_usu;

            if ($id_usu_value<>"") {
                $email=$MUsuario->email;
                Mail::send('sending.mail', ['nombre'=>$MUsuario->nombre,'apellido'=>$MUsuario->apellido,'login'=>$MUsuario->login], function ($message) use ($email) {
                    $message->subject('Te Invitamos!!');
                    $message->to($email);

                },$when);

                MUsusariosSending::where('user_id',$MUsuario->id_usuario)->delete();
                MUsusariosSending::create(['user_id'=>$MUsuario->id_usuario]);
            }
        }
        return redirect('sending');

    }
}
