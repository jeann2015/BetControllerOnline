@extends('layouts.header')
@section('content')
<table class="table table-striped">
<tr>
  <td colspan="9">
    Clients
  </td>
</tr>
  <tr>
      <td colspan="9">
          @foreach ($user_access as $user_acces)
              @if($user_acces->inserts == 1)
                <a href="{{ url('clients/add') }}" class="btn btn-default" role="button">Add </a>
              @else
                <a href="#" class="btn btn-default" role="button">No Add  </a>
              @endif
          @endforeach

          <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>

      </td>
  </tr>
      <tr class="success">
          <td>Id</td>
          <td>Name</td>
          <td>User Create</td>
          <td>Phone</td>
          <td>ID Document</td>
          <td>Modify</td>
          <td>Delete</td>
      </tr>

          @foreach ($clients as $client)

              <tr>
                  <td>{{ $client->id }}</td>
                  <td>{{ $client->name }}</td>
                  <td>{{ $client->name }}</td>
                  <td>{{ $client->phone }}</td>
                  <td>{{ $client->id_document }}</td>
                    @foreach ($user_access as $user_acces)
                      @if($user_acces->modifys == 1)
                        <td><a href="clients/edit/{{ $client->id }}" class="btn btn-success" role="button">Modify</a></td>
                      @else
                        <td><a href="#" class="btn btn-default" role="button">No Modify</a></td>
                      @endif
                      @if($user_acces->deletes==1)
                        <td><a href="clients/delete/{{ $client->id }}" class="btn btn-danger" role="button">Delete</a></td>
                      @else
                        <td><a href="#" class="btn btn-default" role="button">No Delete</a></td>
                      @endif
                    @endforeach
              </tr>
          @endforeach
  </table>
@endsection
