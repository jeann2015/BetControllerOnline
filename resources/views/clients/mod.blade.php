@extends('layouts.header')

@section('content')
{!! Form::open(array('url' => 'clients/update')) !!}

  <table class="table">
    <tr>
        <td colspan="5">

            Modificar Cliente

        </td>
    </tr>
    <tr>
        <td colspan="5">

            <a href="{{ url('clients') }}" class="btn btn-default" role="button">Back </a>

        </td>
    </tr>
        <tr>
            <td>Nombre</td>
            <td> {!! Form::text('name',$clients->name,array('class' => 'form-control','id'=>'name','required')) !!} </td>
        </tr>

        <tr>
            <td>Phone</td>
            <td> {!! Form::text('phone',$clients->phone,array('class' => 'form-control','id'=>'phone','required')) !!} </td>
        </tr>

        <tr>
            <td>Direccion</td>
            <td> {!! Form::text('address',$clients->address,array('class' => 'form-control','id'=>'address','required')) !!} </td>
        </tr>
        <tr>
            <td>ID Document</td>
            <td> {!! Form::text('id_document',$clients->id_document,array('class' => 'form-control','id'=>'id_document','required')) !!} </td>
        </tr>

            <td colspan="2">
                {!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')); !!}
                {!! Form::hidden('id',$clients->id,array('id'=>'id')) !!}
            </td>
            </td>
        </tr>
    </table>
  {!! Form::close() !!}

@endsection
