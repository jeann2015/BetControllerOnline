@extends('layouts.header')

@section('content')
{!! Form::open(array('url' => 'clients/new')) !!}

  <table class="table">
    <tr>
        <td colspan="5">

            Nuevo Cliente

        </td>
    </tr>
    <tr>
        <td colspan="5">

            <a href="{{ url('clients') }}" class="btn btn-default" role="button">Back </a>

        </td>
    </tr>
        <tr>
            <td>Nombre</td>
            <td> {!! Form::text('name','',array('class' => 'form-control','id'=>'name','required')) !!} </td>
        </tr>

        <tr>
            <td>Phone</td>
            <td> {!! Form::text('phone','',array('class' => 'form-control','id'=>'phone','required')) !!} </td>
        </tr>

        <tr>
            <td>Direccion</td>
            <td> {!! Form::text('address','',array('class' => 'form-control','id'=>'address','required')) !!} </td>
        </tr>
        <tr>
            <td>ID Document</td>
            <td> {!! Form::text('id_document','',array('class' => 'form-control','id'=>'id_document','required')) !!} </td>
        </tr>

            <td colspan="2">
                {!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')); !!}
            </td>
        </tr>
    </table>
  {!! Form::close() !!}

@endsection
