@extends('layouts.header')

@section('content')
{!! Form::open(array('url' => 'enviando/new')) !!}

  <table class="table">
    <tr>
        <td colspan="5">
            
            Enviar Correo

        </td>
    </tr>
    <tr>
        <td colspan="5">
            
            <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>

        </td>
    </tr>

      <table id="General">
          <thead>
            <tr>
                <td>ID</td>
                <td>Nombre</td>
                <td>Apellido</td>
                <td>Correo</td>
                <td>Enviado</td>
                <td>Fecha/Hora de Envio</td>
                <td>Enviar</td>
            </tr>
          </thead>
          <tbody>
          @foreach($MUsuarios as $MUsuario)
              <tr>
                  <td>{{$MUsuario->id_usuario}}</td>
                  <td>{{$MUsuario->nombre}}</td>
                  <td>{{$MUsuario->apellido}}</td>
                  <td>{{$MUsuario->email}}</td>
                  @if($MUsusariosSending->count()>0 && $MUsusariosSending->where('user_id',$MUsuario->id_usuario)->count() >0)
                    <td>{{ 'Si Enviado' }}</td>
                    <td>{{$MUsusariosSending->pluck('updated_at')[0]}}</td>
                  @else
                      <td>{{ 'No Enviado' }}</td>
                      <td>{{'Sin fecha'}}</td>
                  @endif
                  <td>
                      <input type="checkbox" class="form-control" onclick="changeValue(this.value,'enviar{{$MUsuario->id_usuario}}')" id="enviar{{$MUsuario->id_usuario}}" name="enviar{{$MUsuario->id_usuario}}" value="0">
                  </td>
              </tr>
          @endforeach
          </tbody>
      </table>

        <tr>
            <td colspan="2">
                {!! Form::submit('Send!',array('class' => 'btn btn-primary','id'=>'send')) !!}
            </td>
        </tr>

    </table>   
  {!! Form::close() !!} 

@endsection