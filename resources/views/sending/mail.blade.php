<!DOCTYPE html>
<!-- saved from url=(0050)https://maradeportes.com/register?action=subscribe -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Mara Deportes te invita!</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device, initial-scale=1 , maximum-scale=1">
</head>
<body>

<div style="padding-left: 15px;padding-right: 15px; left:0; right: 0; bottom: 0; top: 0; position: absolute;  background-color:#eceff1">
    <br><br>
    <table width="100%" style="padding-bottom: 20px; max-width: 600px !important;background-color:#fff" cellpadding="0" cellspacing="0" align="center">

        <tbody><tr>
            <td style="background-color: #00632c;border-bottom: 7px solid #004720;text-align: center;padding:10px;">
                <img src="./Mara Deportes te invita!_files/maradeportes-01-demo-cintillo.png">
            </td>
        </tr>

        <tr>
            <td style=" padding:20px;">
                <div style="box-sizing: content-box;color: #7c7c7c;font-family: &#39;Helvetica Neue&#39;,Helvetica,Arial,sans-serif;font-size: 15px;font-weight: 400;line-height:25px;width:100%">

                    <h1 style="font-size: 22px; font-weight: 600; color:#000">Estimado {{ $nombre." ".$apellido }} <br>(Usuario: {{$login}}):</h1>

                    No pierdas la oportunidad de ganar. Te invitamos a visitar nuestra web donde encontraras la mejor calidad y atención para tus apuestas deportivas.

                </div>
            </td>
        </tr>

        <tr>
            <td style="text-align: center;padding:20px;">
                <a style="font-weight: bold; border-radius: 4px;font-family: Helvetica,Arial,sans-serif;text-decoration: none;box-shadow: 0 2px 0 #046b32;color: #FFF !important;background: #3FB350;padding: 12px 20px 12px 20px !important;text-align: center;border: none;outline: none;cursor: pointer !important;font-size: 16px !important;" target="_blank" href="https://maradeportes.com/register">Inisiar Sesión</a>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;padding:20px;">

                <a style="font-weight: bold;border-radius: 4px;font-family: Helvetica,Arial,sans-serif;text-decoration: none;box-shadow: 0 2px 0 #046b32;color: #FFF !important;background: #3FB350;padding: 12px 20px 12px 20px !important;text-align: center;border: none;outline: none;cursor: pointer !important;font-size: 16px !important;" target="_blank" href="https://maradeportes.com/passwordreset">Olvide mi Contraseña</a>
            </td>
        </tr>
        </tbody></table>



    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContent">
        <tbody>
        <tr>
            <td align="center" valign="top" style="word-break: break-word;color: #656565;font-family: Helvetica;font-size: 13px;line-height: 150%;text-align: center;padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <br>
                <em>Síguenos a través de nuestras redes sociales y está atento a la información suministrada.</em>
                <br>
            </td>
        </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContent">
        <tbody><tr>
            <td align="center" valign="top" style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <tbody><tr>
                        <td align="center" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <!--[if mso]>
                            <table align="center" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                            <![endif]-->

                            <!--[if mso]>
                            <td align="center" valign="top">
                            <![endif]-->


                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <tbody><tr>
                                    <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                            <tbody><tr>
                                                <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                        <tbody><tr>

                                                            <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <a href="http://www.twitter.com/maradeportes" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="./Mara Deportes te invita!_files/color-twitter-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" class=""></a>
                                                            </td>


                                                        </tr>
                                                        </tbody></table>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                </tbody></table>

                            <!--[if mso]>
                            </td>
                            <![endif]-->

                            <!--[if mso]>
                            <td align="center" valign="top">
                            <![endif]-->


                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <tbody><tr>
                                    <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                            <tbody><tr>
                                                <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                        <tbody><tr>

                                                            <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <a href="http://maradeportes.com/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="./Mara Deportes te invita!_files/color-link-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" class=""></a>
                                                            </td>


                                                        </tr>
                                                        </tbody></table>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                </tbody></table>

                            <!--[if mso]>
                            </td>
                            <![endif]-->

                            <!--[if mso]>
                            <td align="center" valign="top">
                            <![endif]-->


                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <tbody><tr>
                                    <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                            <tbody><tr>
                                                <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                        <tbody><tr>

                                                            <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <a href="http://instagram.com/maradeportes" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="./Mara Deportes te invita!_files/color-instagram-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" class=""></a>
                                                            </td>


                                                        </tr>
                                                        </tbody></table>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                </tbody></table>

                            <!--[if mso]>
                            </td>
                            <![endif]-->

                            <!--[if mso]>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        </tbody></table>





    <table width="100%" style="max-width: 600px !important" cellpadding="0" cellspacing="0" align="center">
        <tbody>
        <tr>
            <td style=" padding:20px;">
            </td>
        </tr>
        </tbody>
    </table>

</div>


</body></html>