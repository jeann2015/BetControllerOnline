<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::get('/modules', 'ModulesController@index');
Route::get('/modules/add', 'ModulesController@add');
Route::post('/modules/new', 'ModulesController@news');
Route::get('/modules/edit/{id}', 'ModulesController@edit');
Route::post('/modules/update', 'ModulesController@update');
Route::get('/modules/delete/{id}', 'ModulesController@delete');
Route::post('/modules/destroy', 'ModulesController@destroy');

Route::get('/home', 'HomeController@index');
Route::get('register/verify/{token}','Auth\RegisterController@verify');

Route::get('/users', 'UsersController@index');
Route::get('/users/add', 'UsersController@add');
Route::post('/users/new', 'UsersController@news');
Route::get('/users/edit/{id}', 'UsersController@edit');
Route::post('/users/update', 'UsersController@update');
Route::post('/users/destroy', 'UsersController@destroy');

Route::get('/audit', 'AuditsController@index');

Route::get('/clients', 'ClientsController@index');
Route::get('/clients/add', 'ClientsController@add');
Route::post('/clients/new', 'ClientsController@news');
Route::get('/clients/edit/{id}', 'ClientsController@edit');
Route::post('/clients/update', 'ClientsController@update');
Route::get('/clients/delete/{id}', 'ClientsController@delete');
Route::post('/clients/destroy', 'ClientsController@destroy');

Route::get('/payments', 'PaymentsController@index');

Route::get('/sending', 'SendingController@index');
Route::post('/sending/new', 'SendingController@sending');





